# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 user

DESCRIPTION="Forwarder adding DNSCurve to an authoritative nameserver"
HOMEPAGE="http://curvedns.on2it.net/"
SRC_URI="https://dnscurve.io/software/curve-chroot.diff -> ${PN}.chroot.diff"
EGIT_REPO_URI="https://github.com/curvedns/curvedns.git"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="dev-libs/libev
	virtual/daemontools"
BDEPEND=""

PATCHES=(
	"${DISTDIR}/${PN}.chroot.diff"
)

src_configure() {
	./configure.nacl
	./configure.curvedns amd64
}

src_install() {
	exeinto /usr/bin
	doexe curvedns
	doexe curvedns-keygen
}

pkg_preinst() {
	# The nofiles group is no longer provided by baselayout.
	# Share it with qmail if possible.
	enewgroup nofiles 200

	enewuser curvedns -1 -1 -1 nofiles
}
